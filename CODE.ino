#include <U8glib.h>
U8GLIB_SH1106_128X64 u8g(13, 11, 10, 9); // SCK = 13, MOSI = 11, CS = 10, A0 = 9

int val = 1;
int analogPin = 0;

void setup() {
  Serial.begin(9600);
  
  // flip screen, if required
  //u8g.setRot180();
  // set SPI backup if required
  u8g.setHardwareBackup(u8g_backup_avr_spi);
  // assign default color value
  if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
    u8g.setColorIndex(255); // white
  }
  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
    u8g.setColorIndex(3); // max intensity
  }
  else if ( u8g.getMode() == U8G_MODE_BW ) {
    u8g.setColorIndex(1); // pixel on
  }
  else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
    u8g.setHiColorByRGB(255, 255, 255);
  }
}

int readSensor(){
  int val = 0;
  int val1;
  int val2;
  int val3;
  val1 = analogRead(analogPin);
  delay(10);
  val2 = analogRead(analogPin);
  delay(10);
  val3 = analogRead(analogPin);

  val = (val1 + val2 + val3) / 3;
  return val;
}

int buffSize = 9;
void draw(int in ) {
 // graphic commands to redraw the complete screen should be placed here
 u8g.setFont(u8g_font_unifont);
 //u8g.setFont(u8g_font_osb21);
  char buf[buffSize];
  snprintf (buf, buffSize, "%d", in);
  u8g.drawStr(33, 33, buf);
  //u8g.drawStr( 0, 22, in);
}

void loop(void) {
  int foo = readSensor();
  Serial.println(foo);
  
  u8g.firstPage();
  do {
    draw(foo);
  } while ( u8g.nextPage() );
  delay(50);
}